#pragma once

#include <cmath>


namespace igad
{

	///
	/// A 4d vector/ homogenus vector
	///
	struct Vector4
	{
		union
		{
			// Holds all the values 
			float f[4];

			struct
			{
				/// Holds the value along the x axis
				float x;

				/// Holds the value along the y axis
				float y;

				/// Holds the value along the z axis
				float z;

				float w;
			};
		};
		/// The default constructor creates a zero vector.
		Vector4();

		/// Creates a vector with the given components
		Vector4(float x, float y, float z, float w);

		void UpdateArray();


		/// Returns the value of the given vector added to this
		Vector4 operator+(const Vector4& v) const;

		/// Returns the value of the given vector subtracted from this
		Vector4 operator-(const Vector4& v) const;

		/// Returns a copy of this vector scaled the given value
		Vector4 operator*(const float value) const;

		/// Returns a copy of this vector scaled the inverse of the value
		Vector4 operator/(const float value) const;

		/// Returns the negative this vector
		Vector4 operator-() const;

		/// Calculates and returns the dot product of this
		/// with the given vector
		float operator *(const Vector4& vector) const;

		/// Adds the given vector to this
		void operator+=(const Vector4& v);

		/// Subtracts the given vector from this
		void operator-=(const Vector4& v);

		/// Multiplies this vector by the given scalar
		void operator*=(const float value);

		/* Cross product of a 4D vector's isn't feasible (although, if needed - to be implemented )
		/// Calculates and returns the cross product of this vector
		/// with the given vector
		Vector4 Cross(const Vector4& vector) const;
		*/

		/// Calculates and returns the dot product of this vector
		/// with the given vector
		float Dot(const Vector4& vector) const;

		///  Gets the magnitude of this vector
		float Magnitude() const;

		///  Gets the squared magnitude of this vector
		float SquareMagnitude() const;

		/// Turns a non-zero vector into a vector of unit length
		void Normalize();


		/// Checks if the two vectors have identical components
		bool operator==(const Vector4& other) const;

		/// Checks if the two vectors have non-identical components
		bool operator!=(const Vector4& other) const;

		/// Zero all the components of the vector
		void Clear();

		Vector4 Slerp(float fact, const Vector4& r) const;


	};


	/// Multiplication with Rhs Vector
	inline Vector4 operator*(float val, const Vector4& rhs)
	{
		return rhs * val;
	}
}