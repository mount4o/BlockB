#pragma once


#include <cmath>

namespace igad
{
	///
	/// 2d vector
	///
	struct Vector2
	{
		union
		{
			// Holds all the values 
			float f[2];

			struct
			{
				/// Holds the value along the x axis
				float x;

				/// Holds the value along the y axis
				float y;
			};
		};
		/// The default constructor creates a zero vector.
		Vector2();

		/// Creates a vector with the given components
		Vector2(float x, float y);

		void UpdateArray();


		/// Returns the value of the given vector added to this
		Vector2 operator+(const Vector2& v) const;

		/// Returns the value of the given vector subtracted from this
		Vector2 operator-(const Vector2& v) const;

		/// Returns a copy of this vector scaled the given value
		Vector2 operator*(const float value) const;

		/// Returns a copy of this vector scaled the inverse of the value
		Vector2 operator/(const float value) const;

		/// Returns the negative this vector
		Vector2 operator-() const;

		/// Calculates and returns the dot product of this
		/// with the given vector
		float operator*(const Vector2& vector) const;

		/// Adds the given vector to this
		void operator+=(const Vector2& v);

		/// Subtracts the given vector from this
		void operator-=(const Vector2& v);

		/// Multiplies this vector by the given scalar
		void operator*=(const float value);

		/// Calculates and returns the dot product of this vector
		/// with the given vector
		float Dot(const Vector2& vector) const;

		///  Gets the magnitude of this vector
		float Magnitude() const;

		///  Gets the squared magnitude of this vector
		float SquareMagnitude() const;

		/// Turns a non-zero vector into a vector of unit length
		void Normalize();

		float Cross();

		/// Checks if the two vectors have identical components
		bool operator==(const Vector2& other) const;

		/// Checks if the two vectors have non-identical components
		bool operator!=(const Vector2& other) const;

		/// Zero all the components of the vector
		void Clear();

		Vector2 Slerp(float fact, const Vector2& r) const;

		static float Magnitude(const Vector2& v1, const Vector2& v2);

	};

	///
	/// 3d vector
	///
	struct Vector3
	{
		union
		{
			// Holds all the values 
			float f[3];

			struct
			{
				/// Holds the value along the x axis
				float x;

				/// Holds the value along the y axis
				float y;

				/// Holds the value along the z axis
				float z;
			};
		};

		/// The default constructor creates a zero vector.
		Vector3();

		/// Creates a vector with the given components
		Vector3(float x, float y, float z);

		void UpdateArray();


		/// Returns the value of the given vector added to this
		Vector3 operator+(const Vector3& v) const;

		/// Returns the value of the given vector subtracted from this
		Vector3 operator-(const Vector3& v) const;

		/// Returns a copy of this vector scaled the given value
		Vector3 operator*(const float value) const;

		/// Returns a copy of this vector scaled the inverse of the value
		Vector3 operator/(const float value) const;

		/// Returns the negative this vector
		Vector3 operator-() const;

		/// Calculates and returns the dot product of this
		/// with the given vector
		float operator *(const Vector3& vector) const;

		/// Adds the given vector to this
		void operator+=(const Vector3& v);

		/// Subtracts the given vector from this
		void operator-=(const Vector3& v);

		/// Multiplies this vector by the given scalar
		void operator*=(const float value);

		/// Calculates and returns the cross product of this vector
		/// with the given vector
		Vector3 Cross(const Vector3& vector) const;

		/// Calculates and returns the dot product of this vector
		/// with the given vector
		float Dot(const Vector3& vector) const;

		///  Gets the magnitude of this vector
		float Magnitude() const;

		///  Gets the squared magnitude of this vector
		float SquareMagnitude() const;

		/// Turns a non-zero vector into a vector of unit length
		void Normalize();


		/// Checks if the two vectors have identical components
		bool operator==(const Vector3& other) const;

		/// Checks if the two vectors have non-identical components
		bool operator!=(const Vector3& other) const;

		/// Zero all the components of the vector
		void Clear();

		Vector3 Slerp(float fact, const Vector3& r) const;


	};


	///4d vector
	struct Vector4
	{
		union
		{
			// Holds all the values 
			float f[4];

			struct
			{
				/// Holds the value along the x axis
				float x;

				/// Holds the value along the y axis
				float y;

				/// Holds the value along the z axis
				float z;

				float w;
			};
		};
		/// The default constructor creates a zero vector.
		Vector4();

		/// Creates a vector with the given components
		Vector4(float x, float y, float z, float w);

		void UpdateArray();


		/// Returns the value of the given vector added to this
		Vector4 operator+(const Vector4& v) const;

		/// Returns the value of the given vector subtracted from this
		Vector4 operator-(const Vector4& v) const;

		/// Returns a copy of this vector scaled the given value
		Vector4 operator*(const float value) const;

		/// Returns a copy of this vector scaled the inverse of the value
		Vector4 operator/(const float value) const;

		/// Returns the negative this vector
		Vector4 operator-() const;

		/// Calculates and returns the dot product of this
		/// with the given vector
		float operator *(const Vector4& vector) const;

		/// Adds the given vector to this
		void operator+=(const Vector4& v);

		/// Subtracts the given vector from this
		void operator-=(const Vector4& v);

		/// Multiplies this vector by the given scalar
		void operator*=(const float value);

		/* Cross product of a 4D vector's isn't feasible (although, if needed - to be implemented )
		/// Calculates and returns the cross product of this vector
		/// with the given vector
		Vector4 Cross(const Vector4& vector) const;
		*/

		/// Calculates and returns the dot product of this vector
		/// with the given vector
		float Dot(const Vector4& vector) const;

		///  Gets the magnitude of this vector
		float Magnitude() const;

		///  Gets the squared magnitude of this vector
		float SquareMagnitude() const;

		/// Turns a non-zero vector into a vector of unit length
		void Normalize();


		/// Checks if the two vectors have identical components
		bool operator==(const Vector4& other) const;

		/// Checks if the two vectors have non-identical components
		bool operator!=(const Vector4& other) const;

		/// Zero all the components of the vector
		void Clear();

		Vector4 Slerp(float fact, const Vector4& r) const;


	};


	/// Multiplication with Rhs Vector
	inline Vector2 operator*(float val, const Vector2& rhs)
	{
		return rhs * val;
	}

	/// Multiplication with Rhs Vector
	inline Vector3 operator*(float val, const Vector3& rhs)
	{
		return rhs * val;
	}

	/// Multiplication with Rhs Vector
	inline Vector4 operator*(float val, const Vector4& rhs)
	{
		return rhs * val;
	}

	Vector2 ToVector2(const Vector3& vec);
	Vector3 ToVector3(const Vector2& vec, float z);
	Vector3 ToVector3(const Vector2& vec);

}