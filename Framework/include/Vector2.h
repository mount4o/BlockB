#pragma once

#include <cmath>


namespace igad
{

	///
	/// A 4d vector/ homogenus vector
	///
	struct Vector2
	{
		union
		{
			// Holds all the values 
			float f[2];

			struct
			{
				/// Holds the value along the x axis
				float x;

				/// Holds the value along the y axis
				float y;
			};
		};
		/// The default constructor creates a zero vector.
		Vector2();

		/// Creates a vector with the given components
		Vector2(float x, float y);

		void UpdateArray();


		/// Returns the value of the given vector added to this
		Vector2 operator+(const Vector2& v) const;

		/// Returns the value of the given vector subtracted from this
		Vector2 operator-(const Vector2& v) const;

		/// Returns a copy of this vector scaled the given value
		Vector2 operator*(const float value) const;

		/// Returns a copy of this vector scaled the inverse of the value
		Vector2 operator/(const float value) const;

		/// Returns the negative this vector
		Vector2 operator-() const;

		/// Calculates and returns the dot product of this
		/// with the given vector
		float operator *(const Vector2& vector) const;

		/// Adds the given vector to this
		void operator+=(const Vector2& v);

		/// Subtracts the given vector from this
		void operator-=(const Vector2& v);

		/// Multiplies this vector by the given scalar
		void operator*=(const float value);

		/// Calculates and returns the dot product of this vector
		/// with the given vector
		float Dot(const Vector2& vector) const;

		///  Gets the magnitude of this vector
		float Magnitude() const;

		///  Gets the squared magnitude of this vector
		float SquareMagnitude() const;

		/// Turns a non-zero vector into a vector of unit length
		void Normalize();


		/// Checks if the two vectors have identical components
		bool operator==(const Vector2& other) const;

		/// Checks if the two vectors have non-identical components
		bool operator!=(const Vector2& other) const;

		/// Zero all the components of the vector
		void Clear();

		Vector2 Slerp(float fact, const Vector2& r) const;

	};


	/// Multiplication with Rhs Vector
	inline Vector2 operator*(float val, const Vector2& rhs)
	{
		return rhs * val;
	}
}