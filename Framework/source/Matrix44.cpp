#include <Matrix44.h>



using namespace igad;
Matrix44::Matrix44(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33)
{

	float arr_m[4][4] = { m00, m01, m02, m03,
		m10, m11, m12, m13,
		m20, m21, m22, m23,
		m30, m31, m32, m33 };

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			m[i][j] = arr_m[i][j];
		}
	}
}

Matrix44::Matrix44()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			m[i][j] = 0;
		}
	}

	m[0][0] = 1, m[1][1] = 1, m[2][2] = 1, m[3][3] = 1;
}

float Matrix44::Determinant33(float m11, float m12, float m13, float m21, float m22, float m23, float m31, float m32, float m33) const
{
	return	(m11 * m22 * m33) +
		(m12 * m23 * m31) +
		(m13 * m21 * m32) -
		(m13 * m22 * m31) -
		(m11 * m23 * m32) -
		(m12 * m21 * m33);
}

/* Broken
void Matrix44::UpdateAxisVectors()
{
xAxis = Vector3(m[0][0], m[1][0], m[2][0]);
wx = m[3][0];
yAxis = Vector3(m[0][1], m[1][1], m[2][1]);
wy = m[3][1];
zAxis = Vector3(m[0][2], m[1][2], m[2][2]);
wz = m[3][2];

xAxisH = Vector3(m[0][0], m[0][1], m[0][2]);
wxh = m[0][3];
yAxisH = Vector3(m[1][0], m[1][1], m[1][2]);
wyh = m[1][3];
zAxisH = Vector3(m[2][0], m[2][1], m[2][2]);
wzh = m[2][3];
}
*/
Vector3 Matrix44::operator*(const Vector3 & vec) const
{
	Vector3 vector;
	vector.x = vec.Dot(Vector3(m[0][0], m[1][0], m[2][0])) + m[3][0];
	vector.y = vec.Dot(Vector3(m[0][1], m[1][1], m[2][1])) + m[3][1];
	vector.z = vec.Dot(Vector3(m[0][2], m[1][2], m[2][2])) + m[3][2];

	return vector;
}

Matrix44 Matrix44::operator+(const Matrix44 & mat) const
{
	Matrix44 matrix;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			matrix.m[i][j] = this->m[i][j] + mat.m[i][j];
		}
	}
	return matrix;
}

Matrix44 Matrix44::operator-(const Matrix44 & mat) const
{
	Matrix44 matrix;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			matrix.m[i][j] = this->m[i][j] - mat.m[i][j];
		}
	}

	return matrix;
}

Matrix44 Matrix44::operator*(const Matrix44 & mat) const
{
	Matrix44 matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); 

	for (int row = 0; row < 4; row++)
	{
		for (int column = 0; column < 4; column++)
		{
			for (int inner = 0; inner < 4; inner++)
			{
				matrix.m[row][column] += mat.m[row][inner] * m[inner][column];
			}
		}
	}
	return matrix;
}

Vector3 Matrix44::GetTranslation() const
{
	return Vector3(m[3][0], m[3][1], m[3][2]);
}

void Matrix44::SetTranslation(const Vector3 & vec)
{
	m[3][0] = vec.x, m[3][1] = vec.y, m[3][2] = vec.z;
}

Vector3 Matrix44::GetXAxis() const
{
	return xAxis;
}

Vector3 Matrix44::GetYAxis() const
{
	return yAxis;
}

Vector3 Matrix44::GetZAxis() const
{
	return zAxis;
}

float Matrix44::Determinant() const
{

	float det0, det1, det2, det3;

	det0 = m[0][0] * Determinant33(m[1][1], m[1][2], m[1][3], m[2][1], m[2][2], m[2][3], m[3][1], m[3][2], m[3][3]);
	det1 = m[0][1] * Determinant33(m[1][0], m[1][2], m[1][3], m[2][0], m[2][2], m[2][3], m[3][0], m[3][2], m[3][3]);
	det2 = m[0][2] * Determinant33(m[1][0], m[1][1], m[1][3], m[2][0], m[2][1], m[2][3], m[3][0], m[3][1], m[3][3]);
	det3 = m[0][3] * Determinant33(m[1][0], m[1][1], m[1][2], m[2][0], m[2][1], m[2][2], m[3][0], m[3][1], m[3][2]);

	return det0 - det1 + det2 - det3;
}

bool Matrix44::Invert()
{

	Matrix44 inverse;

	inverse.f[0] = f[5] * f[10] * f[15] -
		f[5] * f[11] * f[14] -
		f[9] * f[6] * f[15] +
		f[9] * f[7] * f[14] +
		f[13] * f[6] * f[11] -
		f[13] * f[7] * f[10];

	inverse.f[4] = -f[4] * f[10] * f[15] +
		f[4] * f[11] * f[14] +
		f[8] * f[6] * f[15] -
		f[8] * f[7] * f[14] -
		f[12] * f[6] * f[11] +
		f[12] * f[7] * f[10];

	inverse.f[8] = f[4] * f[9] * f[15] -
		f[4] * f[11] * f[13] -
		f[8] * f[5] * f[15] +
		f[8] * f[7] * f[13] +
		f[12] * f[5] * f[11] -
		f[12] * f[7] * f[9];

	inverse.f[12] = -f[4] * f[9] * f[14] +
		f[4] * f[10] * f[13] +
		f[8] * f[5] * f[14] -
		f[8] * f[6] * f[13] -
		f[12] * f[5] * f[10] +
		f[12] * f[6] * f[9];

	inverse.f[1] = -f[1] * f[10] * f[15] +
		f[1] * f[11] * f[14] +
		f[9] * f[2] * f[15] -
		f[9] * f[3] * f[14] -
		f[13] * f[2] * f[11] +
		f[13] * f[3] * f[10];

	inverse.f[5] = f[0] * f[10] * f[15] -
		f[0] * f[11] * f[14] -
		f[8] * f[2] * f[15] +
		f[8] * f[3] * f[14] +
		f[12] * f[2] * f[11] -
		f[12] * f[3] * f[10];

	inverse.f[9] = -f[0] * f[9] * f[15] +
		f[0] * f[11] * f[13] +
		f[8] * f[1] * f[15] -
		f[8] * f[3] * f[13] -
		f[12] * f[1] * f[11] +
		f[12] * f[3] * f[9];

	inverse.f[13] = f[0] * f[9] * f[14] -
		f[0] * f[10] * f[13] -
		f[8] * f[1] * f[14] +
		f[8] * f[2] * f[13] +
		f[12] * f[1] * f[10] -
		f[12] * f[2] * f[9];

	inverse.f[2] = f[1] * f[6] * f[15] -
		f[1] * f[7] * f[14] -
		f[5] * f[2] * f[15] +
		f[5] * f[3] * f[14] +
		f[13] * f[2] * f[7] -
		f[13] * f[3] * f[6];

	inverse.f[6] = -f[0] * f[6] * f[15] +
		f[0] * f[7] * f[14] +
		f[4] * f[2] * f[15] -
		f[4] * f[3] * f[14] -
		f[12] * f[2] * f[7] +
		f[12] * f[3] * f[6];

	inverse.f[10] = f[0] * f[5] * f[15] -
		f[0] * f[7] * f[13] -
		f[4] * f[1] * f[15] +
		f[4] * f[3] * f[13] +
		f[12] * f[1] * f[7] -
		f[12] * f[3] * f[5];

	inverse.f[14] = -f[0] * f[5] * f[14] +
		f[0] * f[6] * f[13] +
		f[4] * f[1] * f[14] -
		f[4] * f[2] * f[13] -
		f[12] * f[1] * f[6] +
		f[12] * f[2] * f[5];

	inverse.f[3] = -f[1] * f[6] * f[11] +
		f[1] * f[7] * f[10] +
		f[5] * f[2] * f[11] -
		f[5] * f[3] * f[10] -
		f[9] * f[2] * f[7] +
		f[9] * f[3] * f[6];

	inverse.f[7] = f[0] * f[6] * f[11] -
		f[0] * f[7] * f[10] -
		f[4] * f[2] * f[11] +
		f[4] * f[3] * f[10] +
		f[8] * f[2] * f[7] -
		f[8] * f[3] * f[6];

	inverse.f[11] = -f[0] * f[5] * f[11] +
		f[0] * f[7] * f[9] +
		f[4] * f[1] * f[11] -
		f[4] * f[3] * f[9] -
		f[8] * f[1] * f[7] +
		f[8] * f[3] * f[5];

	inverse.f[15] = f[0] * f[5] * f[10] -
		f[0] * f[6] * f[9] -
		f[4] * f[1] * f[10] +
		f[4] * f[2] * f[9] +
		f[8] * f[1] * f[6] -
		f[8] * f[2] * f[5];

	float det0 = f[0] * inverse.f[0] + f[1] * inverse.f[4] + f[2] * inverse.f[8] + f[3] * inverse.f[12];

	if (det0 == 0)
		return false;

	det0 = 1.0f / det0;

	for (int i = 0; i < 16; i++)
		f[i] = inverse.f[i] * det0;

	return true;
}

void Matrix44::Transpose()
{
	Matrix44 mat;
	for (auto i = 0; i < 4; ++i)
	{
		for (auto j = 0; j < 4; ++j)
		{
			mat.m[j][i] = m[i][j];
		}
	}
	*this = mat;
}

void Matrix44::SetOrientation(const Vector3 & x, const Vector3 & y, const Vector3 & z)
{
	m[0][0] = x.x, m[0][1] = x.y, m[0][2] = x.z;
	m[1][0] = y.x, m[1][1] = y.y, m[1][2] = y.z;
	m[2][0] = z.x, m[2][1] = z.y, m[2][2] = z.z;
}

void Matrix44::SetEulerAxis(float yaw, float pitch, float roll)
{
	*this = CreateIdentity();
	m[0][0] = cosf(yaw) * cosf(pitch) - cosf(roll) * sinf(pitch) * sinf(yaw);
	m[0][1] = cosf(yaw) * sinf(pitch) + cosf(roll) * cosf(pitch) * sinf(yaw);
	m[0][2] = sinf(yaw) * sinf(roll);

	m[1][0] = -sinf(yaw) * cosf(pitch) - cosf(roll) * sinf(pitch) * cosf(yaw);
	m[1][1] = -sinf(yaw) * sinf(pitch) + cosf(roll) * cosf(pitch) * cosf(yaw);
	m[1][2] = cosf(yaw) * sinf(roll);
	
	m[2][0] = sinf(roll) * sinf(pitch);
	m[2][1] = -sinf(roll) * cosf(pitch);
	m[2][2] = cosf(roll);
	Transpose();
}

Matrix44 Matrix44::CreateIdentity()
{

	Matrix44 matrix;
	return matrix;
}

Matrix44 Matrix44::CreateTranslation(float x, float y, float z)
{
	Matrix44 matrix;
	matrix.m[3][0] = x, matrix.m[3][1] = y, matrix.m[3][2] = z;

	return matrix;
}

Matrix44 Matrix44::CreateScale(Vector3 scale)
{
	Matrix44 matrix;
	matrix.m[0][0] = scale.x, matrix.m[1][1] = scale.y, matrix.m[2][2] = scale.z;

	return matrix;
}

Matrix44 Matrix44::CreateRotate(float angle, const Vector3 & axis)
{	
	float pCos = cos(angle);
	float pSin = sin(angle);
	float _pCosN = 1.0f - pCos;

	Matrix44 matrix(pCos + _pCosN * axis.f[0] * axis.f[0],
					_pCosN * axis.f[0] * axis.f[1] + axis.f[2] * pSin,
					_pCosN * axis.f[0] * axis.f[2] - axis.f[1] * pSin,
					0.0f, _pCosN * axis.f[0] * axis.f[1] - axis.f[2] * pSin,
					pCos + _pCosN * axis.f[1] * axis.f[1],
					_pCosN * axis.f[1] * axis.f[2] + axis.f[0] * pSin,
					0.0f, _pCosN * axis.f[0] * axis.f[2] + axis.f[1] * pSin,
					_pCosN * axis.f[1] * axis.f[2] - axis.f[0] * pSin,
					pCos + _pCosN * axis.f[2] * axis.f[2],
					0.0f, 0.0f, 0.0f, 0.0f, 1.0f);


	return matrix;

}

Matrix44 Matrix44::CreateRotateX(float angle)
{
	Matrix44 matrix(1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, cos(angle), sin(angle), 0.0f,
		0.0f, -sin(angle), cos(angle), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	return matrix;
}

Matrix44 Matrix44::CreateRotateY(float angle)
{
	float pCos = cos(angle);
	float pSin = sin(angle);
	Matrix44 matrix(cos(angle), 0.0f, -sin(angle), 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		sin(angle), 0.0f, cos(angle), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	return matrix;
}

Matrix44 Matrix44::CreateRotateZ(float angle)
{
	Matrix44 matrix(cos(angle), sin(angle), 0.0f, 0.0f,
		-sin(angle), cos(angle), 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	return matrix;
}

Matrix44 Matrix44::CreateOrtho(float left, float right, float bottom, float top, float nearZ, float farZ)
{
	Matrix44    projection;
	float       deltaX = right - left;
	float       deltaY = top - bottom;
	float       deltaZ = farZ - nearZ;

	if ((deltaX == 0.0f) || (deltaY == 0.0f) || (deltaZ == 0.0f))
		return projection;

	projection.m[0][0] = 2.0f / deltaX;
	projection.m[1][1] = 2.0f / deltaY;
	projection.m[2][2] = -2.0f / deltaZ;
	projection.m[3][0] = -(right + left) / deltaX;
	projection.m[3][1] = -(top + bottom) / deltaY;
	projection.m[3][2] = -(nearZ + farZ) / deltaZ;

	return projection;
}

Matrix44 Matrix44::CreateFrustum(float left, float right, float bottom, float top, float nearZ, float farZ)
{
	Matrix44    frustum(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0, 0, 0);
	float       deltaX = right - left;
	float       deltaY = top - bottom;
	float       deltaZ = farZ - nearZ;


	if ((nearZ <= 0.0f) || (farZ <= 0.0f) ||
		(deltaX <= 0.0f) || (deltaY <= 0.0f) || (deltaZ <= 0.0f))
		return CreateIdentity();;

	frustum.m[0][0] = 2.0f * nearZ / deltaX;

	frustum.m[1][1] = 2.0f * nearZ / deltaY;

	frustum.m[2][0] = (right + left) / deltaX;
	frustum.m[2][1] = (top + bottom) / deltaY;
	frustum.m[2][2] = -(nearZ + farZ) / deltaZ;
	frustum.m[2][3] = -1.0f;

	frustum.m[3][2] = -2.0f * nearZ * farZ / deltaZ;

	return frustum;
}
	//frustum.m[0][1] = frustum.m[0][2] = frustum.m[0][3] = 0.0f;
	//frustum.m[1][0] = frustum.m[1][2] = frustum.m[1][3] = 0.0f;
	//frustum.m[3][0] = frustum.m[3][1] = frustum.m[3][3] = 0.0f;

Matrix44 Matrix44::CreatePerspective(float fovy, float aspect, float nearZ, float farZ)
{
	float cotan = 1.0f / tanf(fovy / 2.0f);

	Matrix44 matrix(cotan / aspect, 0.0f, 0.0f, 0.0f,
					0.0f, cotan, 0.0f, 0.0f,
					0.0f, 0.0f, (farZ + nearZ) / (nearZ - farZ), -1.0f,
					0.0f, 0.0f, (2.0f * farZ * nearZ) / (nearZ - farZ), 0.0f);
	return matrix; 
}

Matrix44 Matrix44::CreateLookAt(const Vector3 & eye, const Vector3 & center, const Vector3 & up)
{
	//Constructing the camera space 
	Vector3 w = eye - center;
	w.Normalize();

	Vector3 u = up.Cross(w);
	u.Normalize();

	Vector3 v = w.Cross(u);
	v.Normalize();

	Matrix44 rotation(u.x, v.x, w.x, 0,
		u.y, v.y, w.y, 0,
		u.z, v.z, w.z, 0,
		0, 0, 0, 1);

	Matrix44 translation = CreateTranslation(-eye.x, -eye.y, -eye.z);


	Matrix44 view = rotation * translation;

	return view;
}

Vector3 Matrix44::TransformDirectionVector(const Vector3 & direction)
{
	Vector3 result(direction.x * m[0][0] +
		direction.y * m[1][0] +
		direction.z * m[2][0],

		direction.x * m[0][1] +
		direction.y * m[1][1] +
		direction.z * m[2][1],

		direction.x * m[0][2] +
		direction.y * m[1][2] +
		direction.z * m[2][2]);

	return result;
}
