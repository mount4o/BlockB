#include <Vector4.h>


igad::Vector4::Vector4()
{
	x, y, z, w = 0;
	UpdateArray();
}

igad::Vector4::Vector4(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
	UpdateArray();
}

void igad::Vector4::UpdateArray()
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
	f[3] = w;
}

igad::Vector4 igad::Vector4::operator+(const Vector4 & v) const
{
	Vector4 vector;
	vector.x = this->x + v.x;
	vector.y = this->y + v.y;
	vector.z = this->z + v.z;
	vector.w = this->w + v.w;

	return vector;
}

igad::Vector4 igad::Vector4::operator-(const Vector4 & v) const
{
	Vector4 vector;
	vector.x = this->x - v.x;
	vector.y = this->y - v.y;
	vector.z = this->z - v.z;
	vector.w = this->w - v.w;

	return vector;
}

igad::Vector4 igad::Vector4::operator*(const float value) const
{
	Vector4 vector;
	vector.x = this->x * value;
	vector.y = this->y * value;
	vector.z = this->z * value;
	vector.w = this->w * value;

	return vector;
}

igad::Vector4 igad::Vector4::operator/(const float value) const
{
	Vector4 vector;
	vector.x = this->x / value;
	vector.y = this->y / value;
	vector.z = this->z / value;
	vector.w = this->w / value;

	return vector;
}

igad::Vector4 igad::Vector4::operator-() const
{
	Vector4 vector;
	vector.x = this->x * -1;
	vector.y = this->y * -1;
	vector.z = this->z * -1;
	vector.w = this->w * -1;

	return vector;
}

float igad::Vector4::operator*(const Vector4 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y) + (this->z + vector.z) + (this->w + vector.w);
}

void igad::Vector4::operator+=(const Vector4 & v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	this->w += v.w;
}

void igad::Vector4::operator-=(const Vector4 & v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	this->w -= v.w;
}

void igad::Vector4::operator*=(const float value)
{
	this->x *= value;
	this->y *= value;
	this->z *= value;
	this->w *= value;
}
/*
igad::Vector4 igad::Vector4::Cross(const Vector4 & vector) const
{
Vector4 vec;

vec.x = (this->y * vector.z) - (this->z * vector.y);
vec.y = (this->z * vector.x) - (this->x * vector.z);
vec.z = (this->x * vector.y) - (this->y * vector.x);

return vec;
}
*/
float igad::Vector4::Dot(const Vector4 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y) + (this->z + vector.z) + (this->w + vector.w);
}

float igad::Vector4::Magnitude() const
{
	return sqrt((x * x) + (y * y) + (z * z) + (w * w));
}

float igad::Vector4::SquareMagnitude() const
{
	return (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2) + pow(this->w, 2));
}

void igad::Vector4::Normalize()
{
	float magnitude = Magnitude();

	this->x /= magnitude;
	this->y /= magnitude;
	this->z /= magnitude;
	this->w /= magnitude;
}

bool igad::Vector4::operator==(const Vector4 & other) const
{
	if (this->x == other.x && this->y == other.y && this->z == other.z && this->w == other.w)
		return true;
	else
		return false;
}

bool igad::Vector4::operator!=(const Vector4 & other) const
{
	if (this->x == other.x && this->y == other.y && this->z == other.z && this->w == other.w)
		return false;
	else
		return true;
}

void igad::Vector4::Clear()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->w = 0;

	for (auto i = 0; i < 4; i++)
	{
		f[i] = 0;
	}
}

igad::Vector4 igad::Vector4::Slerp(float fact, const Vector4 & r) const
{
	Vector4 v0 = *this;
	// Dot product - the cosine of the angle between 2 vectors.
	float dot = Dot(r);

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD)
	{
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.

		Vector4 result = v0 + fact*(r - v0);
		result.Normalize();
		return result;
	}

	// Clamp it to be in the range of Acos()
	if (dot < -1.0f)
		dot = -1.0f;
	else if (dot > 1.0f)
		dot = 1.0f;

	// Acos(dot) returns the angle between start and end,
	// And multiplying that by percent returns the angle betweend
	// start and the final result.
	float theta_0 = acos(dot);
	float theta = acos(dot) * fact;

	Vector4 RelativeVec = r - v0*dot;
	RelativeVec.Normalize();     // Orthonormal basis

	return v0 * cos(theta) + RelativeVec * sin(theta);
}
