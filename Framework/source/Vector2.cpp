#include <Vector2.h>


igad::Vector2::Vector2()
{
	x, y = 0;
	UpdateArray();
}

igad::Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
	UpdateArray();
}

void igad::Vector2::UpdateArray()
{
	f[0] = x;
	f[1] = y;
}

igad::Vector2 igad::Vector2::operator+(const Vector2 & v) const
{
	Vector2 vector;
	vector.x = this->x + v.x;
	vector.y = this->y + v.y;

	return vector;
}

igad::Vector2 igad::Vector2::operator-(const Vector2 & v) const
{
	Vector2 vector;
	vector.x = this->x - v.x;
	vector.y = this->y - v.y;

	return vector;
}

igad::Vector2 igad::Vector2::operator*(const float value) const
{
	Vector2 vector;
	vector.x = this->x * value;
	vector.y = this->y * value;

	return vector;
}

igad::Vector2 igad::Vector2::operator/(const float value) const
{
	Vector2 vector;
	vector.x = this->x / value;
	vector.y = this->y / value;

	return vector;
}

igad::Vector2 igad::Vector2::operator-() const
{
	Vector2 vector;
	vector.x = this->x * -1;
	vector.y = this->y * -1;

	return vector;
}

float igad::Vector2::operator*(const Vector2 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y);
}

void igad::Vector2::operator+=(const Vector2 & v)
{
	this->x += v.x;
	this->y += v.y;
}

void igad::Vector2::operator-=(const Vector2 & v)
{
	this->x -= v.x;
	this->y -= v.y;
}

void igad::Vector2::operator*=(const float value)
{
	this->x *= value;
	this->y *= value;
}

float igad::Vector2::Dot(const Vector2 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y);
}

float igad::Vector2::Magnitude() const
{
	return sqrt((x * x) + (y * y));
}

float igad::Vector2::SquareMagnitude() const
{
	return (pow(this->x, 2) + pow(this->y, 2));
}

void igad::Vector2::Normalize()
{
	float magnitude = Magnitude();

	this->x /= magnitude;
	this->y /= magnitude;
}

bool igad::Vector2::operator==(const Vector2 & other) const
{
	if (this->x == other.x && this->y == other.y)
		return true;
	else
		return false;
}

bool igad::Vector2::operator!=(const Vector2 & other) const
{
	if (this->x == other.x && this->y == other.y)
		return false;
	else
		return true;
}

void igad::Vector2::Clear()
{
	this->x = 0;
	this->y = 0;

	for (auto i = 0; i < 2; i++)
	{
		f[i] = 0;
	}
}

igad::Vector2 igad::Vector2::Slerp(float fact, const Vector2 & r) const
{
	Vector2 v0 = *this;
	// Dot product - the cosine of the angle between 2 vectors.
	float dot = Dot(r);

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD)
	{
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.

		Vector2 result = v0 + fact*(r - v0);
		result.Normalize();
		return result;
	}

	// Clamp it to be in the range of Acos()
	if (dot < -1.0f)
		dot = -1.0f;
	else if (dot > 1.0f)
		dot = 1.0f;

	// Acos(dot) returns the angle between start and end,
	// And multiplying that by percent returns the angle betweend
	// start and the final result.
	float theta_0 = acos(dot);
	float theta = acos(dot) * fact;

	Vector2 RelativeVec = r - v0*dot;
	RelativeVec.Normalize();     // Orthonormal basis

	return v0 * cos(theta) + RelativeVec * sin(theta);
}
