#include <Vector3.h>

using namespace igad;
Vector2::Vector2()
{
	x, y = 0;
	//UpdateArray();
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
	//UpdateArray();
}

void Vector2::UpdateArray()
{
	f[0] = x;
	f[1] = y;
}

Vector2 Vector2::operator+(const Vector2 & v) const
{
	Vector2 vector;
	vector.x = this->x + v.x;
	vector.y = this->y + v.y;

	return vector;
}

Vector2 Vector2::operator-(const Vector2 & v) const
{
	Vector2 vector;
	vector.x = this->x - v.x;
	vector.y = this->y - v.y;

	return vector;
}

Vector2 Vector2::operator*(const float value) const
{
	Vector2 vector;
	vector.x = this->x * value;
	vector.y = this->y * value;

	return vector;
}

Vector2 Vector2::operator/(const float value) const
{
	Vector2 vector;
	vector.x = this->x / value;
	vector.y = this->y / value;

	return vector;
}

Vector2 Vector2::operator-() const
{
	Vector2 vector;
	vector.x = this->x * -1;
	vector.y = this->y * -1;

	return vector;
}

float Vector2::operator*(const Vector2 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y);
}

void Vector2::operator+=(const Vector2 & v)
{
	this->x += v.x;
	this->y += v.y;
}

void Vector2::operator-=(const Vector2 & v)
{
	this->x -= v.x;
	this->y -= v.y;
}

void Vector2::operator*=(const float value)
{
	this->x *= value;
	this->y *= value;
}

float Vector2::Dot(const Vector2 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y);
}

float Vector2::Magnitude() const
{
	return sqrt((x * x) + (y * y));
}

float Vector2::SquareMagnitude() const
{
	return (pow(this->x, 2) + pow(this->y, 2));
}

void Vector2::Normalize()
{
	float magnitude = Magnitude();
	if (magnitude > 0)
	{
		this->x /= magnitude;
		this->y /= magnitude;
	}
}

float igad::Vector2::Cross()
{
	return this->y - this->x;
}

bool Vector2::operator==(const Vector2 & other) const
{
	if (this->x == other.x && this->y == other.y)
		return true;
	else
		return false;
}



bool Vector2::operator!=(const Vector2 & other) const
{
	if (this->x == other.x && this->y == other.y)
		return false;
	else
		return true;
}

void Vector2::Clear()
{
	this->x = 0;
	this->y = 0;

	for (auto i = 0; i < 2; i++)
	{
		f[i] = 0;
	}
}

Vector2 Vector2::Slerp(float fact, const Vector2 & r) const
{
	Vector2 v0 = *this;
	// Dot product - the cosine of the angle between 2 vectors.
	float dot = Dot(r);

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD)
	{
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.

		Vector2 result = v0 + fact*(r - v0);
		result.Normalize();
		return result;
	}

	// Clamp it to be in the range of Acos()
	if (dot < -1.0f)
		dot = -1.0f;
	else if (dot > 1.0f)
		dot = 1.0f;

	// Acos(dot) returns the angle between start and end,
	// And multiplying that by percent returns the angle betweend
	// start and the final result.
	float theta_0 = acos(dot);
	float theta = acos(dot) * fact;

	Vector2 RelativeVec = r - v0*dot;
	RelativeVec.Normalize();     // Orthonormal basis

	return v0 * cos(theta) + RelativeVec * sin(theta);
}

float igad::Vector2::Magnitude(const Vector2 & v1, const Vector2 & v2)
{
	//return v1.Magnitude() + v2.Magnitude();
	return sqrt(v1.x * v1.x + v1.y * v1.y + v2.x * v2.x + v2.y * v2.y);
}


Vector3::Vector3()
{
	x, y, z = 0;
	//	UpdateArray();
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
	//	UpdateArray();
}

void Vector3::UpdateArray()
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
}

Vector3 Vector3::operator+(const Vector3 & v) const
{
	Vector3 vector;
	vector.x = this->x + v.x;
	vector.y = this->y + v.y;
	vector.z = this->z + v.z;

	return vector;
}

Vector3 Vector3::operator-(const Vector3 & v) const
{
	Vector3 vector;
	vector.x = this->x - v.x;
	vector.y = this->y - v.y;
	vector.z = this->z - v.z;

	return vector;
}

Vector3 Vector3::operator*(const float value) const
{
	Vector3 vector;
	vector.x = this->x * value;
	vector.y = this->y * value;
	vector.z = this->z * value;

	return vector;
}

Vector3 Vector3::operator/(const float value) const
{
	Vector3 vector;
	vector.x = this->x / value;
	vector.y = this->y / value;
	vector.z = this->z / value;

	return vector;
}

Vector3 Vector3::operator-() const
{
	Vector3 vector;
	vector.x = this->x * -1;
	vector.y = this->y * -1;
	vector.z = this->z * -1;

	return vector;
}

float Vector3::operator*(const Vector3 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y) + (this->z + vector.z);
}

void Vector3::operator+=(const Vector3 & v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
}

void Vector3::operator-=(const Vector3 & v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
}

void Vector3::operator*=(const float value)
{
	this->x *= value;
	this->y *= value;
	this->z *= value;
}

Vector3 Vector3::Cross(const Vector3 & vector) const
{
	Vector3 vec;

	vec.x = (this->y * vector.z) - (this->z * vector.y);
	vec.y = (this->z * vector.x) - (this->x * vector.z);
	vec.z = (this->x * vector.y) - (this->y * vector.x);

	return vec;
}

float Vector3::Dot(const Vector3 & vector) const
{
	return  (this->x * vector.x) + (this->y * vector.y) + (this->z * vector.z);
}

float Vector3::Magnitude() const
{
	return sqrt((x * x) + (y * y) + (z * z));
}

float Vector3::SquareMagnitude() const
{
	return (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2));
}

void Vector3::Normalize()
{
	float magnitude = Magnitude();
	if (magnitude > 0)
	{
		this->x /= magnitude;
		this->y /= magnitude;
		this->z /= magnitude;
	}
}

bool Vector3::operator==(const Vector3 & other) const
{
	if (this->x == other.x && this->y == other.y && this->z == other.z)
		return true;
	else
		return false;
}

bool Vector3::operator!=(const Vector3 & other) const
{
	if (this->x == other.x && this->y == other.y && this->z == other.z)
		return false;
	else
		return true;
}

void Vector3::Clear()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;

	for (auto i = 0; i < 3; i++)
	{
		f[i] = 0;
	}
}

Vector3 Vector3::Slerp(float fact, const Vector3 & r) const
{
	Vector3 v0 = *this;
	// Dot product - the cosine of the angle between 2 vectors.
	float dot = Dot(r);

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD)
	{
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.

		Vector3 result = v0 + fact*(r - v0);
		result.Normalize();
		return result;
	}

	// Clamp it to be in the range of Acos()
	if (dot < -1.0f)
		dot = -1.0f;
	else if (dot > 1.0f)
		dot = 1.0f;

	// Acos(dot) returns the angle between start and end,
	// And multiplying that by percent returns the angle betweend
	// start and the final result.
	float theta_0 = acos(dot);
	float theta = acos(dot) * fact;

	Vector3 RelativeVec = r - v0*dot;
	RelativeVec.Normalize();     // Orthonormal basis

	return v0 * cos(theta) + RelativeVec * sin(theta);
}

Vector4::Vector4()
{
	x, y, z, w = 0;
	//UpdateArray();
}

Vector4::Vector4(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
	//UpdateArray();
}

void Vector4::UpdateArray()
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
	f[3] = w;
}

Vector4 Vector4::operator+(const Vector4 & v) const
{
	Vector4 vector;
	vector.x = this->x + v.x;
	vector.y = this->y + v.y;
	vector.z = this->z + v.z;
	vector.w = this->w + v.w;

	return vector;
}

Vector4 Vector4::operator-(const Vector4 & v) const
{
	Vector4 vector;
	vector.x = this->x - v.x;
	vector.y = this->y - v.y;
	vector.z = this->z - v.z;
	vector.w = this->w - v.w;

	return vector;
}

Vector4 Vector4::operator*(const float value) const
{
	Vector4 vector;
	vector.x = this->x * value;
	vector.y = this->y * value;
	vector.z = this->z * value;
	vector.w = this->w * value;

	return vector;
}

Vector4 Vector4::operator/(const float value) const
{
	Vector4 vector;
	vector.x = this->x / value;
	vector.y = this->y / value;
	vector.z = this->z / value;
	vector.w = this->w / value;

	return vector;
}

Vector4 Vector4::operator-() const
{
	Vector4 vector;
	vector.x = this->x * -1;
	vector.y = this->y * -1;
	vector.z = this->z * -1;
	vector.w = this->w * -1;

	return vector;
}

float Vector4::operator*(const Vector4 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y) + (this->z + vector.z) + (this->w + vector.w);
}

void Vector4::operator+=(const Vector4 & v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	this->w += v.w;
}

void Vector4::operator-=(const Vector4 & v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	this->w -= v.w;
}

void Vector4::operator*=(const float value)
{
	this->x *= value;
	this->y *= value;
	this->z *= value;
	this->w *= value;
}
/*
Vector4 Vector4::Cross(const Vector4 & vector) const
{
Vector4 vec;

vec.x = (this->y * vector.z) - (this->z * vector.y);
vec.y = (this->z * vector.x) - (this->x * vector.z);
vec.z = (this->x * vector.y) - (this->y * vector.x);

return vec;
}
*/
float Vector4::Dot(const Vector4 & vector) const
{
	return (this->x * vector.x) + (this->y * vector.y) + (this->z * vector.z) + (this->w * vector.w);
}

float Vector4::Magnitude() const
{
	return sqrt((x * x) + (y * y) + (z * z) + (w * w));
}

float Vector4::SquareMagnitude() const
{
	return (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2) + pow(this->w, 2));
}

void Vector4::Normalize()
{
	float magnitude = Magnitude();
	if (magnitude > 0)
	{
		this->x /= magnitude;
		this->y /= magnitude;
		this->z /= magnitude;
		this->w /= magnitude;
	}
}

bool Vector4::operator==(const Vector4 & other) const
{
	if (this->x == other.x && this->y == other.y && this->z == other.z && this->w == other.w)
		return true;
	else
		return false;
}

bool Vector4::operator!=(const Vector4 & other) const
{
	if (this->x == other.x && this->y == other.y && this->z == other.z && this->w == other.w)
		return false;
	else
		return true;
}

void Vector4::Clear()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->w = 0;

	for (auto i = 0; i < 4; i++)
	{
		f[i] = 0;
	}
}

Vector4 Vector4::Slerp(float fact, const Vector4 & r) const
{
	Vector4 v0 = *this;
	// Dot product - the cosine of the angle between 2 vectors.
	float dot = Dot(r);

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD)
	{
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.

		Vector4 result = v0 + fact*(r - v0);
		result.Normalize();
		return result;
	}

	// Clamp it to be in the range of Acos()
	if (dot < -1.0f)
		dot = -1.0f;
	else if (dot > 1.0f)
		dot = 1.0f;

	// Acos(dot) returns the angle between start and end,
	// And multiplying that by percent returns the angle betweend
	// start and the final result.
	float theta_0 = acos(dot);
	float theta = acos(dot) * fact;

	Vector4 RelativeVec = r - v0*dot;
	RelativeVec.Normalize();     // Orthonormal basis

	return v0 * cos(theta) + RelativeVec * sin(theta);
}

Vector2 igad::ToVector2(const Vector3 & vec)
{
	return Vector2(vec.x, vec.y);
}

Vector3 igad::ToVector3(const Vector2 & vec, float z)
{
	return Vector3(vec.x, vec.y, z);
}

Vector3 igad::ToVector3(const Vector2 & vec)
{
	return Vector3(vec.x, vec.y, 0.0f);
}
