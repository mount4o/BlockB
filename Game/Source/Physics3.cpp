#include <Physics3.h>

namespace igad
{
	void phBody3::integrate(double duration)
	{
		position = position + velocity * duration;
		Vector3 acc = acceleration;
		acc = acc + (force * invMass);	// a = Fm

		velocity = velocity + acc * duration;

		velocity *= powf(damping, duration);
	}

	void phBody3::setMass(double m)
	{
		mass = m;
		invMass = 1 / m;
	}
}