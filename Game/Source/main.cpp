#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include <World.h>

using namespace igad;



static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	World* world = World::GetInstance();
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);


	if(key == GLFW_KEY_UP && action != GLFW_RELEASE)
	{
		world->KEY = KEYS::UP_KEY;
	}
	else if(key == GLFW_KEY_DOWN && action != GLFW_RELEASE)
	{
		world->KEY = KEYS::DOWN_KEY;
	}
	else if(key == GLFW_KEY_LEFT && action != GLFW_RELEASE)
	{
		world->KEY = KEYS::LEFT_KEY;
	}
	else if(key == GLFW_KEY_RIGHT && action != GLFW_RELEASE)
	{
		world->KEY = KEYS::RIGHT_KEY;
	}
	else if (key == GLFW_KEY_Z && action != GLFW_RELEASE)
	{
		world->KEY = KEYS::FIRST_KEY;
	}
	else if (key == GLFW_KEY_X && action != GLFW_RELEASE)
	{
		world->KEY = KEYS::SECOND_KEY;
	}
	else
	{
		world->KEY = KEYS::NO_KEY;
	}
}

int main(int argc, char **argv)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);		 // yes, 3 and 2!!!
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // But also 4 if present
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	window = glfwCreateWindow(1280, 800, "OpenGL", nullptr, nullptr);

	int major = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR);
	int minor = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR);
	int revision = glfwGetWindowAttrib(window, GLFW_CONTEXT_REVISION);
	std::cout << "OpenGL Version " << major << "." << minor << "." << revision << std::endl;

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(window, key_callback);
	
	if (!gladLoadGLLoader(GLADloadproc(glfwGetProcAddress)))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		return -1;
	}

	float ratio;
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	ratio = width / float(height);
	auto lastFrame = glfwGetTime();

	igad::World* world = new igad::World(width, height);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	while (!glfwWindowShouldClose(window))
	{
		auto currentFrame = glfwGetTime();
		auto deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		glViewport(0, 0, width, height);
		glClearColor(1.0, 1.0, 1.0, 1.0);
		//glClear;
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_CULL_FACE);

		world->Update(deltaTime);
		world->Render();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	delete world;
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);

}