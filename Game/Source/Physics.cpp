#include <Physics.h>
#include <World.h>

using namespace igad;

Manager::Manager(std::vector<phBody*> b)
{
	bodies = b;
}

Manager::Manager()
{
}

Manager::~Manager()
{
}

void Manager::ProcessInput(KEYS KEY)
{

	switch (KEY)
	{
	case(KEYS::UP_KEY):
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if(bodies[i]->playerControled == true)
				bodies[i]->addForce(Vector2(0.0f, 65.0f));
		}
	}break;
	case(KEYS::DOWN_KEY):
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i]->playerControled == true)
				bodies[i]->addForce(Vector2(0.0f, -65.0f));
		}
	}break;
	case(KEYS::LEFT_KEY):
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i]->playerControled == true)
				bodies[i]->addForce(Vector2(-65.0f, 0));
		}
	}break;
	case(KEYS::RIGHT_KEY):
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i]->playerControled == true)
				bodies[i]->addForce(Vector2(65.0f, 0));
		}
	}break;
	case(KEYS::NO_KEY):
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i]->playerControled == true)
				bodies[i]->addForce(Vector2(0.0f, 0.0f));
		}
	}break;
	}
}

void Manager::UpdateForces(double duration)
{
	for (size_t i = 0; i < bodies.size(); i++)
	{
	}
}

void Manager::Update(double duration)	
{
	UpdateForces(duration);
	for (size_t i = 0; i < bodies.size(); i++)
	{
		bodies[i]->Update(duration);
		if(bodies[i]->shape->isLegit())
			CheckCollisions(bodies[i], duration);
	}
}

void igad::Manager::CheckCollisions(phBody* body, double duration)
{
	Manifold collision;
	for (size_t i = 0; i < bodies.size(); i++)
	{
		if (collision.DetectCollision(body, bodies[i]))
			collision.ResolveCollision(duration);
	}
}

void igad::Manager::ResolveCollision(Manifold &collision)
{
	if (!collision.resolved)
	{
		phBody* first = collision.body1;
		phBody* second = collision.body2;

		float firstInvMass = first->getInverseMass();
		float secondInvMass = second->getInverseMass();

		Vector2 relativeVelocity = first->getVelocity() - second->getVelocity();

		float velocityNormal = relativeVelocity.Dot(collision.normal);

		if (velocityNormal  > 0)
			return;

		if (first->hasInfiniteMass() && second->hasInfiniteMass())
			return;
		
		float restitution = 0.9f;

		float j = -(1 + restitution) * velocityNormal;

		j /= firstInvMass + secondInvMass;

		Vector2 impulse = j * collision.normal;
		first->linearMotion.velocity += firstInvMass * impulse;
		second->linearMotion.velocity -= secondInvMass * impulse;
		return;
	}
}

void Manager::Flush()
{
	for (size_t i = 0; i < bodies.size(); i++)
	{
		delete bodies[i]->shape;
		delete bodies[i];
	}
}