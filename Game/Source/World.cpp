#include <World.h>
#include <vector>

namespace igad
{
	World* World::m_instance = nullptr;

	World::World(unsigned int Width, unsigned int Height)
	{
		m_instance = this;

		m_width = Width;
		m_height = Height;

		float fov = 60.0f;
		float dist = 150.0f;
		float ratio = (float)(Width / Height);

		m_height = tan(DegToRad(fov * 0.5f)) * dist;
		m_width = m_height * ratio;

		auto shader = new Shader("./Assets/Shaders/BasicVertex.vsh ", "./Assets/Shaders/BasicFragment.fsh");
		m_renderer = new Renderer(shader);
		m_background = new Background();
		m_camera = new Camera();
		physicsManager = new Manager();


		CreateObjects();

		Matrix44 view = Matrix44::CreateLookAt(Vector3(m_width * 0.5f, m_height * 0.5f, dist), Vector3(m_width * 0.5f, m_height * 0.5f, 0), Vector3(0, 1, 0));
		Matrix44 projection = Matrix44::CreatePerspective(DegToRad(fov), ratio, 0.1f, 2000.0f);
		m_camera->SetView(view);
		m_camera->SetProjection(projection);

		tileMap = new TileMap(physicsManager);

		gDebugDraw3D.Initialize();
	}

	World::~World()
	{
		for (size_t i = 0; i < m_objects.size(); i++)
			delete m_objects[i];
		delete m_camera;
		delete m_background;
		delete tileMap;
		physicsManager->Flush();

	}

	void World::CreateObjects()
	{
		std::string texture, sphereMesh, boxMesh;
		texture = "./Assets/Textures/White.png";
		sphereMesh = "./Assets/Models/Sphere.obj";

		float offsetX = 15;
		float offsetY = 18;

		for (size_t j = 0; j < 2; ++j)
		{
			for (size_t i = 0; i < 28; ++i)
			{
				Object* sphere = new Object(sphereMesh, texture);
				Matrix44 sphereTransform;
				sphereTransform.SetTranslation(Vector3(offsetX, offsetY, globalZcord));
				sphere->setTransform(sphereTransform);
				sphere->physicsInit(offsetX, 0.6f, Vector2(1.0f, 1.0f), false);
				physicsManager->addBody(sphere->getBody());
				offsetX += 2;
				m_objects.push_back(sphere);
			}
			offsetX = 15;
			offsetY += 5;
		}

		Object* SPHERE = new Object(sphereMesh, texture);
		Matrix44 SPHEREtransform;
		SPHEREtransform.SetTranslation(Vector3(m_width * 0.5f, m_height* 0.5f, globalZcord));
		SPHERE->setTransform(SPHEREtransform);
		SPHERE->physicsInit(5.0f, 0.6f, Vector2(1.0f, 1.0f), true);
		physicsManager->addBody(SPHERE->getBody());

		m_objects.push_back(SPHERE);
	}

	void World::Update(float dt)
	{
		physicsManager->ProcessInput(KEY);

		for (size_t i = 0; i < m_objects.size(); i++)
		{
			m_objects[i]->Update(dt);
		}

		physicsManager->Update(dt);

	}

	void World::Render()
	{
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);

		m_renderer->Begin(m_camera->View(), m_camera->Projection());

		m_background->Render(m_camera, m_renderer);
		
		tileMap->Render(m_renderer);


		for (size_t i = 0; i < m_objects.size(); i++)
		{
			m_objects[i]->Render(m_camera, m_renderer);
		}

		gDebugDraw3D.Draw(m_camera->Projection() * m_camera->View());
		gDebugDraw3D.Clear();

	}
}
