#include <Graphics.h>
#include <World.h>

using namespace igad;

Object::Object(const std::string & mesh, const std::string & texture)
{
	m_mesh = new Mesh(mesh, vertices);
	m_texture = new Texture(texture);
}

void Object::physicsInit(double mass, double damping, Vector2 shapeScale, bool ControledByPlayer)
{
	m_body = new phBody();
	m_body->setBody(mass, damping, m_transform, vertices, shapeScale, ControledByPlayer);
}


void Object::Update(double deltaTime)
{
	m_transform = m_body->getTransform();
}

Object::~Object()
{
	delete m_mesh;
	delete m_texture;
}

void Object::Render(Camera * camera, Renderer * renderer)
{
	renderer->Render(m_transform, m_texture, m_mesh);
}

Background::Background()
{
	m_mesh = new Mesh();
	m_texture = new Texture("./Assets/Backgrounds/corona_up.png");

	float width = World::GetInstance()->GetWidth();
	float height = World::GetInstance()->GetHeight();

	float fov = 60.0f;
	float dist = 250.0f;
	float ratio = width / height;
	float w = tan(DegToRad(fov * 0.5f)) * dist * ratio;
	float h = w * ratio;

	dist = -25;
	

	std::vector<VertexFormat> vertices =
	{
		{ Vector3(-w, -h, dist),  Vector3(0, 1, 0),{ 0, 0 } },
		{ Vector3(w, -h, dist),  Vector3(0, 1, 0),{ 0, 1 } },
		{ Vector3(w, h, dist),  Vector3(0, 1, 0),{ 1, 1 } },
		{ Vector3(-w, h, dist),  Vector3(0, 1, 0),{ 1, 0 } }
	};
	m_mesh->SetVertices(move(vertices));
	std::vector<GLushort> indices = { 0, 1, 3, 1, 2, 3 };
	m_mesh->SetIndices(move(indices));
	m_mesh->Apply();
}


Background::~Background()
{
	delete m_mesh;
	delete m_texture;
}

void Background::Render(Camera* camera, Renderer* renderer)
{
	renderer->Render(Matrix44::CreateIdentity(), m_texture, m_mesh);
}

igad::Tile::Tile(Mesh* mesh, const Vector2& aPosition, const std::vector<Vector3>& vertices)
{
	m_mesh = mesh;
	m_body = new phBody();
	m_transform = Matrix44::CreateTranslation(aPosition.x, aPosition.y, globalZcord);
	m_body->setBody(0.0f, 1.0f, m_transform, vertices, Vector2(1.0f, 1.0f), false);
}

igad::Tile::~Tile()
{
	delete m_mesh;
}

void igad::Tile::Update()
{
	m_transform = m_body->getTransform();
}

void igad::Tile::Render(Renderer * renderer, Texture* aTexture)
{
	renderer->Render(Matrix44::CreateIdentity(), aTexture, m_mesh);
}

igad::TileMap::TileMap(Manager* physicsManager)
{
	float width = World::GetInstance()->GetWidth();
	float height = World::GetInstance()->GetHeight();

	float UpperLeftX = 10.0f;
	float UpperLeftY = height - 15.0f;
	float TileWidth = width / 100;
	float TileHeight = height / 100;

	std::string texture = "./Assets/Textures/white.png";
	std::string mesh = "./Assets/Models/Box.obj";

	for (size_t row = 0; row < 25; ++row)
	{
		for (size_t column = 0; column < 25; ++column)
		{
			if (tile_map[row][column] == 1)
			{
				Object* firstBox = new Object(mesh, texture);
				Matrix44 firstBox_transform;
				firstBox_transform.SetTranslation(Vector3(TileWidth + UpperLeftX, TileHeight + UpperLeftY, globalZcord));
				firstBox->setTransform(firstBox_transform);
				firstBox->physicsInit(0.0f, 0.6f, Vector2(1.0f, 1.0f), false);
				physicsManager->addBody(firstBox->getBody());

				World::GetInstance()->AddObject(firstBox);
			}

			UpperLeftX += 2.5f;
		}
		UpperLeftX = 10.0f;
		UpperLeftY -= 2.5f;
	}

}

igad::TileMap::~TileMap()
{
}

void igad::TileMap::AddBodiesToManager(Manager * physicsManager)
{
}

std::vector<Vector3> igad::TileMap::ScreenToWorld(const std::vector<Vector3>& vertices, Camera * camera)
{
	std::vector<Vector3> result;
	for (size_t i = 0; i < vertices.size(); ++i)
	{
			Matrix44 inverse = camera->ViewProjection();
			Vector3 shit = inverse * vertices[i];
			result.push_back(shit);
		
	}

	return result;
}

void igad::TileMap::Render(Renderer * renderer)
{
}
