#include <Collision.h>
#include <phBody.h>

using namespace igad;
bool igad::Shape::isLegit()
{

	if (circle.radius == -FLT_MAX)
		return false;

	if (circle.center.x == -FLT_MAX || circle.center.y == -FLT_MAX)
		return false;

	if (aabb.max.x == -FLT_MAX || aabb.max.y == -FLT_MAX)
		return false;
		
	if (aabb.min.x == FLT_MAX || aabb.min.y == FLT_MAX)
		return false;

	return true;
}
void Shape::computeCollisionShapes(const Matrix44 & transform)
{
	computeCircle(transform);
	computeAABB(transform);
}

	void Shape::computeAABB(const Matrix44& transform)
	{
		Vector2 lower = ToVector2(Vector3(transform * vertices[0]));
		Vector2 upper = lower;

		for (size_t i = 0; i < vertices.size(); ++i)
		{
			Vector2 v = ToVector2(Vector3(transform * vertices[i]));

			if (upper.x > v.x && upper.y > v.y)
			{
				upper = v;
			}

			if (lower.x < v.x && lower.y < v.y)
			{
				upper = v;
			}

		}
		aabb.position = ToVector2(transform.GetTranslation());
		aabb.min = lower - aabb.position;
		aabb.max = upper + aabb.position;
	}

void Shape::computeCircle(const Matrix44 & transform)
{
	circle.center = ToVector2(transform.GetTranslation());

	Vector2 upper = ToVector2(Vector3(transform * vertices[0]));
	for (size_t i = 1; i < vertices.size(); ++i)
	{
		Vector2 v = ToVector2(Vector3(transform * vertices[i]));
		if (upper.x > v.x && upper.y > v.y)
		{
			upper = v;
		}	
	}
	Vector2 dst = upper - circle.center;
	dst = ToVector2(m_circleScale * ToVector3(dst));
	circle.radius = dst.Magnitude();
}

void igad::Shape::init()
{
	circle.Clear();
	aabb.Clear();
}

void Shape::Update(const Matrix44 & transform)
{
	circle.center = ToVector2(transform.GetTranslation());
	aabb.position = ToVector2(transform.GetTranslation());
	Vector2 r(circle.radius, circle.radius);
	r = ToVector2(m_boxScale * ToVector3(r));
	aabb.min = aabb.position - r;
	aabb.max = r + aabb.position;
}

void Shape::DebugRender(float z)
{
	Color orange = Color::Orange;
	Color purple = Color::Purple;


	Vector3 A(aabb.min.x, aabb.min.y, z);
	Vector3 B(aabb.max.x, aabb.min.y, z);
	Vector3 C(aabb.max.x, aabb.max.y, z);
	Vector3 D(aabb.min.x, aabb.max.y, z);

	gDebugDraw3D.AddLine(A, B, orange);
	gDebugDraw3D.AddLine(B, C, orange);
	gDebugDraw3D.AddLine(C, D, orange);
	gDebugDraw3D.AddLine(D, A, orange);
}

Shape::Shape(const std::vector<Vector3>& v, Matrix44 transform, Vector2 circleScale, Vector2 boxScale)
{
	vertices = v;
	m_circleScale = Matrix44::CreateScale(ToVector3(circleScale, 1.0f));
	m_boxScale = Matrix44::CreateScale(ToVector3(boxScale, 0.0f));

	computeCollisionShapes(transform);
}

Shape::Shape(const std::vector<Vector3>& v, Matrix44 transform, Vector2 scale)
{
	vertices = v;

	Matrix44 s = Matrix44::CreateScale(ToVector3(scale, 1));
	m_boxScale = s;
	m_circleScale = s;

	computeCollisionShapes(transform);
}


Shape::~Shape()
{
}

void igad::Manifold::ResolveCollision(double duration)
{
	if (!resolved)
	{
		phBody* first = body1;
		phBody* second = body2;


		float firstInvMass = first->getInverseMass();
		float secondInvMass = second->getInverseMass();

		Vector2 relativeVelocity = first->getVelocity() - second->getVelocity();

		float velocityNormal = relativeVelocity.Dot(normal);

		if (velocityNormal  > 0)
			return;

		float restitution = 0.9f;

		float j = -(1 + restitution) * velocityNormal;

		j /= firstInvMass + secondInvMass;

		float i = j;
		i /= first->getInertia() + second->getInertia();

		Vector2 impulse = j * normal;
		Vector2 angularImpulse = i * normal;

		const float percent = 0.01f;
		Vector2 posCorrect = normal * penDepth * percent;

		first->transformData.position2 += posCorrect;
		second->transformData.position2 -= posCorrect;


		first->linearMotion.velocity += firstInvMass * impulse;
		second->linearMotion.velocity -= secondInvMass * impulse;

		//Applying the impulse to the angular motion just didn't seem right when there is a 
		//body of infinite mass involved. And if the collision was continious the object didn't
		//stop spinning.
		if (!first->hasInfiniteMass() && !second->hasInfiniteMass())
		{
			first->angularMotion.velocity += first->getInertia() * impulse.Cross() * duration;
			second->angularMotion.velocity -= second->getInertia() * impulse.Cross() * duration;
		}
		return;
	}
}

bool igad::Manifold::DetectCollision(phBody* first, phBody* second)
{
	if (first != second)
	{
		if (first->hasInfiniteMass() && second->hasInfiniteMass())
			return false;

		body1 = first;
		body2 = second;

		AABB firstBox = first->shape->getAABB();
		AABB secondBox = second->shape->getAABB();

		Circle firstCircle = first->shape->getCircle();
		Circle secondCircle = second->shape->getCircle();

		Vector2 n = first->shape->getCircle().center - second->shape->getCircle().center;
		Vector2 contactPoint;

		float ax_extent = (firstBox.max.x - firstBox.min.x) * 0.5f;
		float bx_extent = (secondBox.max.x - secondBox.min.x)* 0.5f;

		float ay_extent = (firstBox.max.y - firstBox.min.y) * 0.5f;
		float by_extent = (secondBox.max.y - secondBox.min.y) * 0.5f;

		float x_overlap = ax_extent + bx_extent - abs(n.x);
		float y_overlap = ay_extent + by_extent - abs(n.y);

		int firstType;
		int secondType;

		if ((firstBox.min.x <= secondBox.max.x) && (firstBox.min.y <= secondBox.max.y) && (firstBox.max.x >= secondBox.min.x) && (firstBox.max.y >= secondBox.min.y))
		{

			float r = firstCircle.radius + secondCircle.radius;
			Vector2 distance = firstCircle.center - secondCircle.center;
			float dist = distance.Magnitude();
			if (r > dist)

			if (pow((firstCircle.radius + secondCircle.radius), 2) > pow((firstCircle.center.x - secondCircle.center.x), 2) +
				pow((firstCircle.center.y - secondCircle.center.y), 2))
			{
				if (x_overlap < y_overlap)
				{
					penDepth = x_overlap;
					if (n.x < 0)
						normal = Vector2(-1.0f, 0.0f);
					else
						normal = Vector2(1.0f, 0.0f);

					return true;
				}

				if (y_overlap < x_overlap)
				{
					penDepth = y_overlap;
					if (n.y < 0)
						normal = Vector2(0.0f, -1.0f);
					else
						normal = Vector2(0.0f, 1.0f);

					return true;
				}

			}
		}
	}
		return false;
}
