#include <phBody.h>

using namespace igad;

phBody::phBody()
{
	//Setting mass data
	massData.mass = 0;
	massData.invMass = 0;

	//Setting properties needed for linear motion calculations
	linearMotion.velocity.Clear();
	linearMotion.forceAccum.Clear();
	linearMotion.acceleration.Clear();

	//Setting properties needed for angular motion calculations
	angularMotion.orientation = 0.0f;
	angularMotion.velocity = 0.0f;
}

phBody::~phBody()
{
}
void phBody::addForce(const Vector2 & f)
{
	if (!massData.infiniteMass)
	{
		linearMotion.forceAccum += f;
		angularMotion.torqueAccum += f;
		angularMotion.torque += linearMotion.forceAccum.Cross();
	}
}

void phBody::clearAccumulators()
{
	linearMotion.forceAccum.Clear();
	angularMotion.torqueAccum.Clear();
}

void phBody::calculateTransform()
{
	transformData.transformMat = Matrix44::CreateTranslation(transformData.position2.x, transformData.position2.y, transformData.transformMat.m[3][2]);
	transformData.transformMat = transformData.transformMat * Matrix44::CreateRotateZ(angularMotion.orientation);
}

void phBody::Update(double duration)
{
	integrate(duration);
	shape->Update(transformData.transformMat);
}

void phBody::DebugRender() const
{
	shape->DebugRender(transformData.z);
}

bool phBody::hasInfiniteMass()
{
	return massData.infiniteMass;

}

void phBody::integrate(double duration)
{
	assert(duration > 0.0);
	if (!massData.infiniteMass)
	{
		double drag = powf(damping, duration);
		
		linearMotion.acceleration = linearMotion.forceAccum * massData.invMass;

		linearMotion.velocity = linearMotion.velocity + linearMotion.acceleration  * duration;

		angularMotion.acceleration = massData.intertia * angularMotion.torqueAccum.Cross() * duration;

		angularMotion.velocity += angularMotion.acceleration * duration;
		angularMotion.orientation += angularMotion.velocity * duration;
		transformData.position2 = transformData.position2 + linearMotion.velocity * duration;


		linearMotion.velocity *= drag;
		angularMotion.velocity *= drag;

		calculateTransform();
		clearAccumulators();
	}
}

void phBody::setBody(double mass, double damping, const Matrix44& transform, const std::vector<Vector3>& v, Vector2 circleScale, Vector2 boxScale, bool ControledByPlayer)
{
	setMass(mass);
	playerControled = ControledByPlayer;

	//Setting position values
	transformData.transformMat = transform;
	transformData.position2.x = transform.GetTranslation().x;
	transformData.position2.y = transform.GetTranslation().y;
	transformData.z = transform.GetTranslation().z;

	// 15 is a value I found works best with the time-step I'm using. This may be different , not exactly tested.
	massData.intertia = 15 * massData.invMass;

	this->damping = damping;

	//Creating the shape and setting it up in the debug renderer
	shape = new Shape(v, transform, circleScale, boxScale);
	DebugRender();
}

void phBody::setBody(double mass, double damping, const Matrix44& transform, const std::vector<Vector3>& v, Vector2 shapeScale, bool ControledByPlayer)
{
	setMass(mass);
	playerControled = ControledByPlayer;

	//Setting position values
	transformData.transformMat = transform;
	transformData.position2.x = transform.GetTranslation().x;
	transformData.position2.y = transform.GetTranslation().y;
	transformData.z = transform.GetTranslation().z;

	//Calculation of inertia along the z axis acording to the mass. Currently not useful and not included.

	// 15 is a value I found works best with the time-step I'm using. This may be different , not exactly tested.
	massData.intertia = 15 * massData.invMass;

	this->damping = damping;

	//Creating the shape and setting it up in the debug renderer
	shape = new Shape(v, transform, shapeScale);
	DebugRender();
}

void phBody::setMass(double m)
{
	if (m == 0)
	{
		massData.mass = 0;
		massData.invMass = 0;
		massData.infiniteMass = true;
	}
	else
	{
		massData.mass = m;
		massData.invMass = 1 / m;
		massData.infiniteMass = false;
	}
}