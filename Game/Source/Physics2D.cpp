#include <Physics2D.h>

PhysicsObject::PhysicsObject(Shape::AABB shape, float dens)
{
	body.m_shape->m_aabb = shape;
	body.m_data.m_mass = dens;
}

PhysicsObject::PhysicsObject(Shape::Circle shape, float dens)
{
}

PhysicsObject::~PhysicsObject()
{
}

void PhysicsObject::Update(float dt)
{
}
/*
bool Physics::Collision(PhysicsObject & a, PhysicsObject & b)
{
	bool aabbColl, circleColl = false;

	if (a.aabb.active == true && b.aabb.active == true)
	{
		aabbColl = AABBcollision(a.aabb, b.aabb);
	}
	if (a.circle.active == true && b.circle.active == true)
	{
		circleColl = CircleCollision(a.circle, b.circle);
	}

	return aabbColl, circleColl;
}

bool Physics::AABBcollision(Shape::AABB a, Shape::AABB b)
{
	if (a.max.x < b.min.x || a.min.x > b.max.x) return false;
	if (a.max.y < b.min.y || a.min.y > b.max.y) return false;

	return true;
}

bool Physics::CircleCollision(Shape::Circle a, Shape::Circle b)
{
	float r = a.m_radius + b.radius;
	r *= r;
	return r > pow((a.position.x + b.position.x), 2) + pow((a.position.y + b.position.y), 2);

}

*/