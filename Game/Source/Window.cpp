#include <Window.h>
#include <iostream>

void windowResize(GLFWwindow* window, int width, int height);

graphics::Window::Window(const char * name, int width, int height)
{
	m_Title = name;
	m_Width = width;
	m_Height = height;
	if (!init())
		glfwTerminate();
}

graphics::Window::~Window()
{
	glfwTerminate();
}

bool graphics::Window::init()
{
	if (!glfwInit())
	{
		std::cout << "Failed to initialize GLFW" << std::endl;
		return false;
	}

	m_Window = glfwCreateWindow(m_Width, m_Height, m_Title, NULL, NULL);
	if (!m_Window)
	{
		std::cout << "Failed to init GLFW window" << std::endl;
		return false;
	}

	glfwSwapInterval(1);
	glfwMakeContextCurrent(m_Window);
	
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);		
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	int major = glfwGetWindowAttrib(m_Window, GLFW_CONTEXT_VERSION_MAJOR);
	int minor = glfwGetWindowAttrib(m_Window, GLFW_CONTEXT_VERSION_MINOR);
	int revision = glfwGetWindowAttrib(m_Window, GLFW_CONTEXT_REVISION);

	std::cout << "OpenGL Version " << major << "." << minor << "." << revision << std::endl;

	glfwGetFramebufferSize(m_Window, &width, &height);
	ratio = width / float(height);
	
	glfwSetKeyCallback(m_Window, key_callback);
	glfwSetWindowSizeCallback(m_Window, windowResize);
	return true;

}

void graphics::Window::clear() const
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void graphics::Window::clear(float cValue1, float cValue2, float cValue3, float cValue4) const
{
	glClearColor(cValue1, cValue2, cValue3, cValue4);
	glClear(GL_COLOR_BUFFER_BIT);
}


void graphics::Window::proccessInput()
{
	if (glfwGetKey(m_Window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(m_Window, true);
}


void graphics::Window::update()
{
	proccessInput();
	glfwPollEvents();
	glfwSwapBuffers(m_Window);
}

bool graphics::Window::closed() const
{
	return glfwWindowShouldClose(m_Window);
}


void windowResize(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

