#pragma once
#include <Graphics.h>

namespace igad
{
	const float globalZcord = 90.0f;

	class World
	{
	private:
		unsigned int m_width, m_height;
		Renderer* m_renderer;
		Camera* m_camera;
		Background* m_background;
		Matrix44 m_transform;
		static World* m_instance;
		std::vector<Object*> m_objects;
		Manager* physicsManager;
		TileMap* tileMap;


	public:
		KEYS KEY;
	public:
		World(unsigned int Width, unsigned int Height);
		~World();


		static World* GetInstance() { return m_instance; }
		std::vector<Object*> GetBodies() { return m_objects; }

		float GetWidth() { return (float)m_width; }
		float GetHeight() { return (float)m_height; }

		void AddObject(Object* object) { m_objects.push_back(object); }

		//Updates the game world(graphics and physics)
		void Update(float dt);
		//Renders the game world
		void Render();


	private:
		// Creates all the game objects
		void CreateObjects();
	};

}