#pragma once
#include <glad\glad.h>
#include <GLFW\glfw3.h>


namespace graphics
{
	class Window
	{
	private:
		const char* m_Title;
		int m_Width, m_Height;
		GLFWwindow* m_Window;
		bool m_Closed;
		int major, minor, revision;

	public:
		float ratio;
		int width, height;


	public:
		Window(const char* name, int width, int height);
		~Window();
		void clear() const;
		void clear(float cValue1, float cValue2, float cValue3, float cValue4) const;
		void update();
		bool closed() const;
		void proccessInput();
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GL_TRUE);
		}

		inline int GetWidth() { return m_Width; }
		inline int GetHeight() { return m_Height; }
		inline GLFWwindow* GetWindowHandle() { return m_Window; }

	private:
		bool init();
	};
}


