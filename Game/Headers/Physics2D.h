#pragma once
#include <Vector3.h>

class Physics;

struct Shape
{
	struct AABB
	{
		bool m_active = false;
		igad::Vector2 m_min;
		igad::Vector2 m_max;
	}m_aabb;

	struct Circle
	{
		bool m_active = false;
		float m_radius;
		igad::Vector2 m_position;
	}m_circle;
};

struct MassData
{
	float m_mass;
	float m_inv_mass;
	float m_inertia;
	float m_inv_inertia;
};
struct Material
{
	float m_density;
	float m_restitution;
};


struct PhysicsBody
{
	Shape* m_shape;
	Material m_material;
	MassData m_data;
	igad::Vector2 m_velocity;
	igad::Vector2 m_force;
	float m_gravity;
};


class PhysicsObject
{
public:
	PhysicsBody body;
public:
	PhysicsObject(Shape::AABB shape, float dens);
	PhysicsObject(Shape::Circle shape, float dens);

	~PhysicsObject();
	
	void Update(float dt);

};

class Physics
{
public:
	static bool Collision(PhysicsObject& a, PhysicsObject& b);
	static bool AABBcollision(Shape::AABB a, Shape::AABB b);
	static bool CircleCollision(Shape::Circle a, Shape::Circle b);
};