#pragma once
#include <Physics.h>

namespace igad
{
	struct phBody3
	{
		Vector3 position;
		Vector3 velocity;
		Vector3 force;
		Vector3 acceleration;
		double damping;
		double invMass;
		double mass;

		void integrate(double duration);
		void setMass(double m);
	};
}