#pragma once
#include <assert.h>
#include <phBody.h>

namespace igad
{
	enum KEYS;

	class Manager
	{
	private:
		friend class phBody;
		std::vector<phBody*> bodies;
	public:
		//The automatic way. Takes all the bodies and initilizes the
		Manager(std::vector<phBody*> b);

		//The manual way of doing it. It requires that every body has putted
		//itself in the manager and is ready to be initalized.
		//Call "Init()" to initilize all default fources.
		Manager();
		~Manager();

		void addBody(phBody* body) { bodies.push_back(body); }
		
		void ProcessInput(KEYS KEY);
		void UpdateForces(double duration);
		void Update(double duration);
		void CheckCollisions(phBody* body, double duration);
		void ResolveCollision(Manifold &collision);
		void Flush();
	};
}