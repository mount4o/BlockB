#pragma once
#include <Matrix44.h>
#include <iostream>

namespace igad
{
	template<class T>
	const T ClampMax(const T& a, const T& b)
	{
		return (a < b) ? b : a;
	}

	template<class T>
	const T ClampMin(const T& a, const T& b)
	{
		return (a > b) ? b : a;
	}

/////////////////////////////////////////////////////////
// Physic body structures
/////////////////////////////////////////////////////////
	struct Transform
	{
		Matrix44 transformMat;
		Vector2 position2;
		float z;
	};

	struct Material
	{
		float density;
		float restitution;
	};

	struct MassData
	{
		float mass;
		float invMass;
		bool infiniteMass;

		float intertia;
	};

	struct LinearMotion
	{
		Vector2 velocity;
		Vector2 forceAccum;
		Vector2 acceleration;
	};

	struct AngularMotion
	{
		float orientation; // radians
		float velocity;
		float acceleration;
		Vector2 torqueAccum;
		float torque;
	};

/////////////////////////////////////////////////////////
// Collision structures
/////////////////////////////////////////////////////////
	struct AABB
	{
		Vector2 min;
		Vector2 max;
		Vector2 position;
		float width;
		float height;
		float diagonal;
		
		void Clear()
		{
			min.x = FLT_MAX;
			min.y = FLT_MAX;
			max.x = -FLT_MAX;
			max.y = -FLT_MAX;
		}
		bool Overlap(AABB* other)
		{
			return (min.x <= other->max.x) && (min.y <= other->max.y) &&
				(max.x >= other->min.x) && (max.y >= other->min.y);
		}
	};

	struct Circle
	{
		Vector2 center;
		float radius;
		void Clear()
		{
			center.x = -FLT_MAX;
			center.y = -FLT_MAX;
			radius = -FLT_MAX;
		}
		bool Overlap(Circle* other)
		{
			return pow((radius + other->radius), 2) > pow((center.x - other->center.x), 2) +
				pow((center.y - other->center.y), 2);
		}
	};

}
		/*
		bool Overlap(Circle* circle)
		{
			float x0 = pow((circle->center.x - min.x), 2) + pow((circle->center.y - min.y), 2);
			float x1 = pow((circle->center.x - max.x), 2) + pow((circle->center.y - max.y), 2);
			float x = x0;
			if (x1 < x0)
				x = x1;

			return pow((circle->radius + circle->radius), 2) > x;
		}
		*/

		/*
		bool Overlap(AABB* aabb)
		{	
			Vector2 Delta;
			float x = std::max(aabb->min.x, std::min(center.x, aabb->min.x + aabb->width));
			float y = std::max(aabb->min.y, std::min(center.y, aabb->min.y + aabb->height));;

			Delta.x = center.x - x;
			Delta.y = center.y - y;
			//float shit = Delta.SquareMagnitude();

			if (radius > Delta.Magnitude())
				return true;
			else
				return false;
			
		}
		*/
//Algorithm for constructing a convex shape from a set of vertices
/*
float CCW(const Vector2& p1, const Vector2& p2, const Vector2& p3)
{
return (p2.x - p1.x)*(p3.y - p1.y) - (p2.y - p1.y)*(p3.x - p1.x);
}

std::vector<Vector2> GrahamScan(std::vector<Vector2>& vertices)
{
if (vertices.size() == 3)
return vertices;

std::vector<Vector2> hull;
int pivot = 0;

for (size_t i = 0; i < vertices.size(); i++)
{
if (vertices[i].y < vertices[pivot].y)
pivot = i;
}
std::swap(vertices[0], vertices[pivot]);

const Vector2 startingPoint = vertices[0];

std::sort(vertices.begin() + 1, vertices.end(),
[startingPoint](const Vector2 & vec1, const Vector2 & vec2)
{
Vector2 v1 = vec1 - startingPoint;
Vector2 v2 = vec2 - startingPoint;

float f0 = atan2(v1.y, v1.x);
float f1 = atan2(v2.y, v2.x);


return f0 > f1;
});

vertices.erase(unique(vertices.begin(), vertices.end()), vertices.end());

hull.push_back(vertices[0]);
hull.push_back(vertices[1]);
hull.push_back(vertices[2]);


for (size_t i = 2; i < vertices.size(); i++)
{
Vector2& n = vertices[i];
do
{
size_t m = hull.size();
Vector2& p = hull[m - 2];
Vector2& c = hull[m - 1];
if (CCW(p, c, n) > 0.0f)
hull.resize(m - 1);
else
break;
} while (true);
hull.push_back(n);
}

return hull;
}
}
*/
