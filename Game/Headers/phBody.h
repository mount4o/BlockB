#pragma once
#include <Collision.h>

namespace igad
{

	class phBody
	{
	private:
		friend class Manager;
	public:
		Shape* shape;
		Matrix44 translation;
		Transform transformData;
		bool playerControled;
		LinearMotion linearMotion;
		AngularMotion angularMotion;
	private:
		MassData massData;
		double damping;

		void addForce(const Vector2& f);
		void clearAccumulators();

	public:
		//True if the body has infinite mass, false if not
		phBody();
		~phBody();

		//Calculates the transform matrix for rendering an object accordingly to it's physics body
		//In order to preserve the "z" coordinate of an object the method requires it to be sent
		void calculateTransform();

		void Update(double duration);
		void DebugRender() const;
		bool hasInfiniteMass();
		void integrate(double duration);
		void setMass(double m);
		void setBody(double mass, double damping, const Matrix44& transform, const std::vector<Vector3>& v, Vector2 circleScale, Vector2 boxScale, bool ControledByPlayer);
		void setBody(double mass, double damping, const Matrix44& transform, const std::vector<Vector3>& v, Vector2 shapeScale, bool ControledByPlayer);
		void setPosition(Vector2 position) { transformData.position2 = position; }
		Vector2 getPosition() { return transformData.position2; }
		Matrix44 getTransform() { return transformData.transformMat; }
		Vector2 getVelocity() { return linearMotion.velocity; }
		float getMass() { return massData.mass; }
		float getInverseMass() { return massData.invMass; }
		float getInertia() { return massData.intertia; }
	};
}