#pragma once
#include <Defines.h>
#include <Mesh.h>
#include <Texture.h>
#include <Renderer.h>
#include <DebugRenderer.h>
#include <Physics.h>
#include <iostream>

namespace igad
{
	enum KEYS
	{
		NO_KEY,
		FIRST_KEY,
		SECOND_KEY,
		UP_KEY,
		DOWN_KEY,
		LEFT_KEY,
		RIGHT_KEY
	};

	struct Box
	{
		Vector3 A;
		Vector3 B;
		Vector3 C;
		Vector3 D;
	};

	class World;

	class Camera
	{
	public:
		Camera() {}
		const Matrix44& View() const { return m_view; }
		const Matrix44& Projection() const { return m_projection; }
		Matrix44 ViewProjection() const { return m_projection * m_view; }
		void SetProjection(const Matrix44& p) { m_projection = Matrix44::CreateIdentity(), m_projection = p; }
		void SetView(const Matrix44& v) { m_view = v; }

	protected:
		Matrix44	m_view;
		Matrix44	m_projection;
	};

	class Object
	{
	public:
		Object(const std::string & mesh, const std::string & texture);
		~Object();
		Matrix44 getTransform() { return m_transform; }
		void setTransform(Matrix44& t) { m_transform = t; };

		//Initializes the physics body of the object 
		//For the scale - set 1.0f if no scaling needs be done
		void physicsInit(double mass, double damping, Vector2 shapeScale, bool ControledByPlayer);
		phBody* getBody() { return m_body; }

		void Update(double deltaTime);
		void Render(Camera* camera, Renderer* renderer);

	protected:
		Mesh* m_mesh = nullptr;
		Texture* m_texture = nullptr;
		phBody*	m_body = nullptr;
		std::vector<Vector3> vertices;
		Matrix44 m_transform;
		Vector3	scale;
	};

	// Background
	class Background
	{
	public:
		Background();
		~Background();
		void Render(Camera* camera, Renderer* renderer);

	private:
		Mesh*		m_mesh = nullptr;
		Texture*	m_texture = nullptr;
		unsigned int tilemap[9][16];
	};

	class Tile
	{
	private:
		phBody* m_body;
		Matrix44 m_transform;
		Mesh* m_mesh;

	public:
		Tile(Mesh* mesh, const Vector2& aPosition, const std::vector<Vector3>& vertices);
		~Tile();

		void Update();
		void Render(Renderer* renderer, Texture* aTexture);
		phBody* getBody() { return m_body; }
	};

	class TileMap
	{
	public:

		friend class Tile;
		TileMap(Manager* physicsManager);
		~TileMap();

		void AddBodiesToManager(Manager* physicsManager);
		std::vector<Vector3> ScreenToWorld(const std::vector<Vector3>& vertices, Camera* camera);
		void Render(Renderer* renderer);
	private:

		unsigned int m_width;
		unsigned int m_height;
		Texture* m_texture;
		std::vector<Tile*> m_tiles;
		unsigned int tile_map[25][25] = 
		{
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			
		};
	};
}