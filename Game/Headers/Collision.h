#pragma once
#include <Matrix44.h>
#include <vector>
#include <DebugRenderer.h>
#include <phStructs.h>

namespace igad
{

	class phBody;
	class Shape
	{
	public:
		//Create using a transform matrix, scale (pass 1.0f if no scaling needs be done) and the type of collision shape
		// CIRCLE - circle only
		// BOX - aabb only
		// ALL - both 
		Shape(const std::vector<Vector3>& v, Matrix44 transform, Vector2 circleScale, Vector2 boxScale);

		//Create using a transform matrix, scale (pass 1.0f if no scaling needs be done) and the type of collision shape
		// CIRCLE - circle only
		// BOX - aabb only
		// ALL - both 
		Shape(const std::vector<Vector3>& v, Matrix44 transform, Vector2 scale);


		~Shape();


		bool isLegit();
		void computeCollisionShapes(const Matrix44& transform);
		void computeAABB(const Matrix44& transform);
		void computeCircle(const Matrix44& transform);
		void init();

		void Update(const Matrix44& transform);
		void DebugRender(float z);

		AABB getAABB() { return aabb; }
		Circle getCircle() { return circle; }

	private:
		AABB aabb;
		Circle circle;
		std::vector<Vector3> vertices;
		Matrix44 m_circleScale;
		Matrix44 m_boxScale;
	};

	struct Manifold
	{
		Manifold()
		{
			body1 = nullptr;
			body2 = nullptr;
			normal = Vector2();
			position = Vector2();
			penDepth = 0.0f;
			resolved = false;
		}

		void ResolveCollision(double duration);
		bool DetectCollision(phBody* first, phBody* second);

		phBody* body1;
		phBody* body2;
		Vector2 normal;
		Vector2 position;
		float penDepth;
		bool resolved;
	};
}
