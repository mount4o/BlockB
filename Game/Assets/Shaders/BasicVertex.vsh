#version 430 core

//Uniforms
uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_model;

//Attributes
in vec2 a_texture;
in vec3 a_position;
in vec3 a_normal;

//Lighting variables
vec3 lightColor = vec3(0.5, 0.5, 0.5);
vec3 lightDir = vec3(2.0, 3.0, 2.0);


// Vertex shader outputs
out vec2 v_texture;
out vec4 lighting;

void main()
{
	v_texture = a_texture;
	gl_Position = u_projection * u_view * u_model * vec4(a_position, 1.0);

	vec3 normal = mat3(transpose(inverse(u_model))) * a_normal;
	vec3 light_dir = normalize(lightDir);
	float diff = max(dot(normal, lightDir), 0.0);
	lighting = vec4(diff * lightColor, 1.0);
}

