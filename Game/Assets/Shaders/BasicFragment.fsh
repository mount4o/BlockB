#version 430 core

//Input values 
in vec2 v_texture;
in vec4 lighting;

//Uniforms
uniform sampler2D u_texture;


//Output values
out vec4 fragColor;

void main()
{

	fragColor = texture(u_texture, v_texture) * lighting;
}

